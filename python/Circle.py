import RPi.GPIO as GPIO
from AlphaBot2 import AlphaBot2
import time



Button = 7
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(Button,GPIO.IN,GPIO.PUD_UP)

bot = AlphaBot2()
bot.stop()

while GPIO.input(Button) != 0:
    time.sleep(0.5)

while True:
    bot.forward()
    time.sleep(0.5)

    bot.right()
    time.sleep(0.1)



